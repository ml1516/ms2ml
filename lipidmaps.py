def to_subclass(id, suffix='(?)'):
    """
    :param id: lipidmaps ID or anything that starts with a lipidmaps ID, including filenames
    :param suffix: string to append
    :return: the subclass shorthand notation as used in lipidblast
    """
    lookup_table = {
        "GL0201": "DG",
        "GL0301": "TG",
    }
    return lookup_table[id[2:8].upper()] + suffix
