from collections import namedtuple
import csv
import json
import sys

GnpsSpectrum = namedtuple('GnpsSpectrum', ['mzs', 'ints', 'weight'])


def gnpslib_to_json(raw_text):
    """
    convert gnps library dump into json string (list of dicts)
    """
    gnps_spectra = raw_to_gnps_spectrum(raw_text)
    dicts = [gnps_spectrum_to_dict(gnps_spec) for gnps_spec in gnps_spectra]
    return json.dumps(dicts)


def raw_to_gnps_spectrum(line_iterable):
    lines = list(line_iterable)
    i, weight_str = next((i, s) for (i, s) in enumerate(lines) if s.startswith("PEPMASS="))
    del lines[:i]

    i = next(idx for (idx, line) in enumerate(lines) if line[0] in map(str, range(10)))
    j = i + next(idx for (idx, line) in enumerate(lines[i:]) if line[0] not in map(str, range(10)))
    data_lines = lines[i:j]
    reader = csv.reader(data_lines, delimiter=' ')

    tuples = [(float(mz), float(int_)) for (mz, int_) in reader]
    mzs, ints = zip(*tuples)

    weight = weight_str.split('=')[1:]

    spectrum = GnpsSpectrum(mzs=mzs, ints=ints, weight=weight)
    return spectrum


def gnps_spectrum_to_dict(gnps_spec):
    return {
        'mzs': gnps_spec.mzs,
        'ints': gnps_spec.ints,
        'weight': gnps_spec.weight
    }


def dict_to_gnps_spectrum(d):
    return GnpsSpectrum(**d)


if __name__ == '__main__':
    print gnpslib_to_json(open(sys.argv[1]))
