from collections import defaultdict, OrderedDict


def peak_class_counts(X, y, unique_mzs):
    """ Map each peak to how often it occurs in each class. """
    histo_dict = OrderedDict()
    for mz, col in zip(unique_mzs, X.T):
        histo_dict[mz] = bag(y[col.toarray()[0]])
    return histo_dict


def bag(iterable):
    bag = defaultdict(int)
    for it in iterable:
        bag[it] += 1
    return dict(**bag)
