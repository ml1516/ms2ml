import csv
import re
import sys
from collections import namedtuple, defaultdict
from itertools import chain

import numpy as np

compound = "Compound"
Compound = namedtuple(compound, ('name', 'id', 'weight', 'precursor_mz', 'formula', 'mzs', 'ints', 'pk_comms'))

valid_sidechain_char_matcher = re.compile('[\d:/]+')


def subclass_abundances(compounds, level=None):
    """ Return a dict mapping each lipid subclass to how many compounds were found from the respective class. """
    level = level or assign_subclass
    bag = defaultdict(int)
    for comp in compounds:
        bag[level(comp)] += 1
    return dict(**bag)


def assign_subclass(compound):
    if compound.id:
        return compound.id.split(' ')[0]
    elif compound.name.startswith('Cer('):
        return 'Cer-d'
    else:
        return compound.name.split('(')[0]


def parse_all(dump, sep='\r\n'):
    """ Given lipid blast as pure text, return the list of compound objects.
    """
    return [parse_single(single.strip().split(sep)) for single in dump.strip().split(2*sep)]


def unique_peaks(compound_list):
    """ Flatten the mzs arrays, then return the sorted unique values. """
    return np.unique(np.fromiter(chain.from_iterable(mol.mzs for mol in compound_list), dtype=float))


def parse_single(lines):
    name, id_ = parse_name(lines[0])
    mw = float(lines[1].split(': ')[1])
    precursor_mz = float(lines[2].split(': ')[1])
    sum_formula = lines[3].split('; ')[-1]
    r = csv.reader(lines[5:], delimiter=' ')
    mzs, ints, comm = zip(*r)
    mzs, ints = map(float, mzs), map(float, ints)
    return Compound(name, id_, mw, precursor_mz, sum_formula, mzs, ints, comm)


def parse_name(name_str):
    split = name_str.split(': ')[1].split('; ')
    if len(split) == 3:
        id_, _, name = split
        if name.startswith('Cer('):
            id_ = None
        elif id_.startswith('CE('):
            id_, name = None, id_
    elif len(split) == 4:
        if split[0].startswith('HexaAcyl-LipidA-PP'):
            id_, name, _, _ = split
            id_ = id_[9:]
        else:
            id_, _, _, name = split
    return name, id_


def assign_molecular_subspecies(compound):
    def tuple_cmp(t1, t2):
        c1 = cmp(t1[0], t2[0])
        if c1 != 0:
            return c1
        else:
            return cmp(t1[1], t2[1])
    structural_subspecies, subclass = assign_structural_subspecies(compound, return_subclass=True)
    bracket_idcs = find_righmost_balanced_bracket_indices(structural_subspecies)
    sidechains = structural_subspecies[bracket_idcs[0] + 1:bracket_idcs[1] - 1].split('/')
    sorted_tuples = sorted((map(int, ch.split(':')) for ch in sidechains), cmp=tuple_cmp)
    new_bracket_content = '_'.join(':'.join(map(str, tup)) for tup in sorted_tuples)
    return "{}({})".format(subclass, new_bracket_content)


def assign_structural_subspecies(compound, return_subclass=False):
    subclass = assign_subclass(compound)
    s = compound.name
    s = s.split(' ')[0]  # remove comments in CE subclass
    sidechain_content_idcs = find_righmost_balanced_bracket_indices(s)
    s = s[sidechain_content_idcs[0] + 1:sidechain_content_idcs[1] - 1]
    s = remove_all_brackets(s)
    s = remove_extra_characters(s)
    s = "{}({})".format(subclass, s)
    if return_subclass:
        s = s, subclass
    return s


def find_righmost_balanced_bracket_indices(s):
    counter = 0
    l = r = None
    for i, c in enumerate(s[::-1]):
        assert counter >= 0
        if c == '(':
            counter -= 1
        elif c == ')':
            counter += 1
            if not r:
                r = len(s) - i
        if counter == 0 and r is not None:
            l = len(s) - (i + 1)
            break
    return l, r


def remove_all_brackets(s):
    l, r = find_righmost_balanced_bracket_indices(s)
    while l and r:
        s = s[:l] + s[r:]
        l, r = find_righmost_balanced_bracket_indices(s)
    return s


def remove_extra_characters(s):
    return ''.join(valid_sidechain_char_matcher.findall(s))


if __name__ == "__main__":
    with open(sys.argv[1]) as fp:
        compounds = parse_all(fp.read())
    unique_peaks = unique_peaks(compounds)
    abundances = subclass_abundances(compounds)
    print("{} individual peaks were found in {} ms/ms spectra from {} lipid subclasses.".format(
        len(unique_peaks), len(compounds), len(abundances.keys())))
    print("Class abundances:\n{}".format(abundances))
