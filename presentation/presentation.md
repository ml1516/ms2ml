$include(theme.yaml)
$include(metadata.yaml)

#### $titlepage
---
overtheme:
  - slide:
    - content:
      - padding: '3%'
---
$box
$style[width:100%;height:20%;background:#822433;]
$content[color:white;text-align:center;]{
$title[display:block;font-size:200%;padding-top:2%;]
$subtitle[display:block;font-size:120%;padding-top:2%;]
$logo[height:50px;]
}
$endbox

$box
$style[height:30%;]
$content{}
$endbox

$box
$style[width:100%;]
$content[text-align:center;]{
a presentation by $authors[display:block;font-size:150%]
$emails[display:block;font-size:90%;]
$affiliations[display:block;]
}
$endbox

$box
$style[width:100%;padding-top:2%;]
$content[text-align:center;color:#822433;]{
$conference[display:block;font-size:150%;]
$session[display:block;font-size:120%;]
$location[display:block;font-size:90%;text-align:right;padding-right:5%;padding-top:5%;]
$date[display:block;font-size:90%;text-align:right;padding-right:5%;]
}
$endbox



## Intro
### Intro
#### State of the Art
##### Similarity search
+ Spectral database is searched
+ Best match is chosen

##### Problems
+ Spectral databases are small
+ Only molecules within the database can be searched

=> **Many molecules cannot be identified.**

##### Solution
+ Predict metadata rather than exact matches
+ Use machine learning



#### Basic Idea
$columns
$column[width:20%;]
$note
$caption(none){First step}
$content{Train classifier with data from spectral database}
$endnote
$column
$figure
$content[padding:1% 5%;width:120%;box-shadow: 5px 5px 3px rgba(200,200,200,0.3);border-radius:15px]{../img/001.png}
$caption{Training with spectral database}
$endfigure
$endcolumns

$columns
$column[width:20%;]
$note
$caption(none){Second step}
$content{Predict class for experimental data}
$endnote
$column
$figure
$content[padding:1% 5%;width:120%;box-shadow: 5px 5px 3px rgba(200,200,200,0.3);border-radius:15px;]{../img/002.png}
$caption{Prediction with experimental data}
$endfigure
$endcolumns



## Theory
### Lipids
#### Lipid Structure
$columns
$column[width:30%]
$note
$caption(none){Lipid structure}
$content{
+ Black part: Glycerol backbone.
+ Colored parts: Side chains (fatty acids).
}
$endnote
$column
$figure
$content[width:80%;]{../img/lipid_conceptual.jpg}
$caption{Lipid structure}
$endfigure
$endcolumns
##### Glycerol backbone
+ Common to all lipids
##### Sidechains
+ At least one, at most three
+ Determine the category the lipid falls into



#### Lipid Categories
$table
$caption{Lipid categories}
$content{
|Level                 | Example
|----------------------|----------------------------------------|
|Category              | Glycerophospholipid                    |
|Main Class            | Glycerophosphocholine                  |
|Sub Class             | Monoalkylmonoacylglycerophosphocholine |
|Species               | PC(O-36:5)                             |
|Molecular subspecies  | PC(O-16:1\_20:4)                       |
|Structural subspecies | PC(P-16:0/20:4)                        |
|Isomeric subspecies   | PC(P-16:0/20:4(5Z,8Z,11Z,14Z))         |
}
$endtable



### Tandem Mass Spectrometry
#### Tandem Mass Spectrometry
$columns
$column[width:80%;]
$figure
$content{../img/MS2.png}
$caption(none){}
$endfigure
$column[width:20%;]
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$figure
$content{../img/fragmentation_spectrum_edited.png}
$caption(none){}
$endfigure
$endcolumns



## Feature Selection
### Feature Selection
#### Feature Selection
##### Spectrum encoding
$columns
$column
$figure
$content{../img/bar_coding.png}
$caption{Binary feature selection}
$endfigure
$column
$note
$caption(none){One-hot encoding}
$content{
1. Collect all m/z values
2. Sort
3. For each spectrum,
    1. Create a vector of zeros
    2. Find the indices of its m/z values
    3. Put a one at these indices
}
$endnote
$endcolumns

##### Binning
Simple but effective: **Rounding**

##### Thresholding
Several m/z thresholds were tried out



## Data
### Lipidblast
#### Lipidblast
##### Lipidblast
Synthetic spectral database:

+ Fragmentation process simulated based on rules
+ Subset of Lipidmaps
+ Quite large for a spectral database

$columns
$column
$table
$caption{Feature space size}
$content{
Rounding (decimals) | Unique peaks
--------------------|-------------
5 (=None)           | 38273
4                   | 12685
3                   | 8394
2                   | 7144
1                   | 3568
0                   | 1662
}
$endtable
$column
$note
$caption{}
$content{Lipidblast has 212 685 spectra from 119 341 compounds.

It has spectra from 29 out of 354 existing lipid subclasses.

Smallest subclass: 33 spectra}
$endnote
$endcolumns



### MCF
#### MCF
$columns
$column
$note
$caption(none){}
$content{
Real `#PrasadApproved` spectra from the Core Facility!

+ 95 spectra
+ However, majority of classes not present in Lipidblast
+ Noisier than expected
}
$endnote
$column
$figure
$caption{Prasad}
$content{../img/prasad_approved.png}
$endfigure
$endcolumns



#### t-SNE plot
$columns
$column
$figure
$caption{t-SNE plot after sampling 851 vectors and reducing to 50 features with PCA}
$content{../img/tsne.png}
$endfigure
$column[width=20%;]
$note
$caption(none){Observation}
$content{
+ Some clusters are recognizable
+ Some classes look more clustered than others
}
$endnote
$endcolumns



## Classification
### Classification
#### Noise-free Lipidblast
$columns
$column
$table
$caption{Classification rates on pure Lipidblast data}
$content{
    Classifier       | Precision [%] | t/fold [s]
    -----------------|---------------|-----------
    Naive Bayes      | 94.92         | 0.87
    Decision tree    | 95.1          | 5932.79
    Random forest    | 95.2          | 818.97
    Extra trees      | 95.45         | 666.26
    Perceptron       | 96.82         | 13.01
    SVM (linear)     | 97.04         | 32.9
    Ridge regression | 97.1          | 36.48
}
$endtable
$column
$note
$content{
+ No noise, no binning
+ 5-fold cross validation
+ Global precision measure
}
$endnote
$endcolumns

**=> Linear models dominate**



#### Noisy Lipidblast
$columns
$column
$column
$note
$caption(none){Artificial noise}
$content{
+ "Chemical" noise: Add random peaks
+ "Measurement" noise: Gaussian noise on the peak position}
$endnote
$endcolumns

$figure
$content{../img/BernoulliNB_1_rounding_None.png}
$caption(none){}
$endfigure



#### Noisy Lipidblast
$columns
$column
$note
$caption(none){Noise reduction}
$content{
+ Rounding}
$endnote
$column
$note
$caption(none){Artificial noise}
$content{
+ "Measurement" noise: Gaussian noise on the peak position}
$endnote
$endcolumns

$figure
$content{../img/BernoulliNB_1_added_peaks_0.png}
$caption(none){}
$endfigure



#### Noisy Lipidblast
$columns
$column
$note
$caption(none){Noise reduction}
$content{
+ Rounding}
$endnote
$column
$note
$caption(none){Artificial noise}
$content{
+ "Chemical" noise: Add random peaks
+ "Measurement" noise: Gaussian noise on the peak position}
$endnote
$endcolumns

$figure
$content{../img/BernoulliNB_1_rounding_2.png}
$caption(none){}
$endfigure



#### Noisy Lipidblast
$columns
$column
$note
$caption(none){Noise reduction}
$content{
+ Rounding}
$endnote
$column
$note
$caption(none){Artificial noise}
$content{
+ "Measurement" noise: Gaussian noise on the peak position}
$endnote
$endcolumns

$figure
$content{../img/Classifiers_3_added_peaks_0.png}
$caption(none){}
$endfigure



#### Real Data
$columns
$column
$note
$caption(none){Noise reduction}
$content{
+ Rounding
+ m/z threshold}
$endnote
$column
$note
$caption(none){Artificial noise}
$content{
+ "Chemical" noise: Add random peaks}
$endnote
$endcolumns

$columns
$column
$figure
$content{../img/RealData2_LinearSVC_heatmap_0peaks.png}
$caption(none){}
$endfigure
$column
$figure
$content{../img/RealData2_LinearSVC_heatmap_15peaks.png}
$caption(none){}
$endfigure
$endcolumns



## Conclusions
### Conclusions
#### Conclusions
+ Chemical noise mote critical than expected
+ Still promising because of good evaluation on Lipidblast

##### Future Work:
+ Find more noise-tolerant approaches
+ Try different feature selection:
    + More intelligent binning
    + Consider intensities
    + Use distances between peaks
    + n-gram
+ Predict species
+ Apply to other molecule classes