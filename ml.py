from bisect import bisect_left
from itertools import chain

import numpy as np
from scipy.sparse import lil_matrix

import parse_lipidblast as pl


def fs_absolute(compounds, sparse=True, level='subclass'):
    y_func = {'subclass': pl.assign_subclass, 'mol_subspecies': pl.assign_molecular_subspecies,
              'str_subspecies': pl.assign_structural_subspecies}
    y = np.array([y_func[level](comp) for comp in compounds])
    labels = np.unique(y)
    unique_mzs = pl.unique_peaks(compounds)
    X = _finish_fs(unique_mzs, compounds, sparse)
    return X, y, unique_mzs, labels


def fs_round(compounds, decimals=5, sparse=True, level='subclass'):
    """ Same as fs_absolute, but handles noise by rounding to the specified number of decimals. """
    new_compounds = [pl.Compound(name=c.name, id=c.id, weight=c.weight, precursor_mz=c.precursor_mz,
                                 formula=c.formula, mzs=[round(mz, decimals) for mz in c.mzs], ints=c.ints,
                                 pk_comms=c.pk_comms) for c in compounds]
    return fs_absolute(new_compounds, sparse=sparse, level=level)


def fs_binning_hist(compounds, bins=10000, sparse=True):
    """ Use binning as provided by np.histogram. """
    y = np.array([pl.assign_subclass(comp) for comp in compounds])
    labels = np.unique(y)
    compound_mzs = np.fromiter(chain.from_iterable(mol.mzs for mol in compounds), dtype=float)
    _, edges = np.histogram(compound_mzs, bins=bins)
    X = _finish_fs(edges[1:], compounds, sparse)
    return X, y, edges, labels


def add_gauss_noise(compounds, mean=0, sigma=1e-6):
    return [pl.Compound(name=c.name, id=c.id, weight=c.weight, precursor_mz=c.precursor_mz,
                        formula=c.formula, mzs=[mz + np.random.normal(mean, sigma) for mz in c.mzs], ints=c.ints,
                        pk_comms=c.pk_comms) for c in compounds]


def _finish_fs(unique_mzs, compounds, sparse):
    n, d = len(compounds), len(unique_mzs)
    data_structure = lil_matrix if sparse else np.zeros
    x = data_structure((n, d), dtype=np.bool_)
    for i, comp in enumerate(compounds):
        x[i, [bisect_left(unique_mzs, mz) for mz in comp.mzs]] = 1
    if sparse:
        x = x.tocsr()
    return x
