"""
a class that simplifies loading, graphing, sorting and filtering of results
"""
import ClassificationTest
import numpy as np
import os


class ResultPlayer:

    def __init__(self, pickles):
        """

        :param pickles: list of files to load
        :return:
        """
        assert len(pickles) > 0
        self.loaded_pickles = pickles

        self.tests, self.results, self.classification_tests = \
            ResultPlayer._load_data(pickles)

        self.data, self.data_names = self._build_data()

    def group_by(self, data, field, key=None):
        """
        groups the data into a iterable format
        :param data: the data to be grouped
        :param field: what field to group by
        :param key: optional, if field is a dict, look up at this key
        :return: when iterating of returned, each iteration will be (group, value of grouped by,running index)
        """
        column = self.data_names.index(field)

        if key is None:
            unique_values = np.unique(data[:, column])
        else:
            unique_values = np.unique([c[key] for c in data[:, column]])

        groups = []
        for val in unique_values:
            groups.append(self.filter(data, field, val, key=key))

        return zip(groups, unique_values, range(len(unique_values)))

    def filter(self, data, field, val, key=None):
        """
        filter data by parameter
        :param data: the data to be filterd
        :param field: the field to be filtered for
        :param val: the value to filter for
        :param key: optional: compare value only with the dict entry at key key of field field
        :return: the filtered data
        """

        column = self.data_names.index(field)

        if key is None:
            select_i = np.where([c == val for c in data[:, column]])
        else:
            select_i = np.where([c[key] == val for c in data[:, column]])

        return data[select_i, :][0]

    def _build_data(self):

        fieldnames = list(self.tests[0]._fields)
        fieldnames += ["correct_rate", "std"]

        # check that all tests were completed
        uncompleted = sum([r.status != "success" for r in self.results])

        for r, i in zip(reversed(self.results), reversed(range(len(self.results)))):
            if r.status != 'success':
                del self.results[i]
                del self.tests[i]

        if uncompleted != 0:
            print("{} tests were ignored since they were not successfully completed".format(uncompleted))

        tests_array = np.asarray(self.tests)
        rates = np.array([[np.mean(r.correct_rate)] for r in self.results])
        errors = np.array([[np.std(r.correct_rate)] for r in self.results])

        data = np.concatenate((tests_array, rates, errors), axis=1)

        # clean up data
        for f in data[:, 5]:
            if 'add_random_peaks' not in f:
                f['add_random_peaks'] = 0.
            if 'peak_position' not in f:
                f['peak_position'] = 0.
        for t in data[:, 6]:
            if 'round' not in t:
                t['round'] = None

        return data, fieldnames

    def result_tables(self):
        """

        :return: the result table of all loaded tests
        """
        tables = [t.result_table() for t in self.classification_tests]
        return ''.join('%s\n\n' % t for t in tables)

    @staticmethod
    def _load_data(files):
        """
        import pickles using ClassificationTest
        :param files: list of pickle file to load
        :return:
        """
        # check that all files exist
        assert sum([not os.path.isfile("{}.npz".format(f)) for f in files]) == 0

        t = []
        tests = []
        results = []
        for f in files:
            c = ClassificationTest.ClassificationTest(save=f)
            t.append(c)
            tests += c.tests
            results += c.results
        return tests, results, t



