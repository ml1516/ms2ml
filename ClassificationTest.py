"""
Class to test the classifiers, add noise and store/display results (test_classifier rewritten object-oriented)
"""
from __future__ import print_function
from collections import namedtuple
import numpy as np
from sklearn.cross_validation import StratifiedKFold, train_test_split
from sklearn.metrics import confusion_matrix
from tabulate import tabulate
from time import time
from time import ctime
import matplotlib.pyplot as plt
import ml
import os.path
import sys
import pickle
from copy import deepcopy

Test = namedtuple("Test", ("classifier_name", "classifier_input", "n_folds", "test_size",
                           "feature_type", "noise", "transformation", "description"))
Result = namedtuple("Result", ("status", "run_time", "correct_rate", "confusion"))


class ClassificationTest:
    def __init__(self,
                 compounds=None,
                 compounds_test=None,
                 save=None):
        """
        initialize a test set
        :param compounds: list of namedtuple of compounds (needed to generate noisy data)
        :param compounds_test: list of namedtuples of compounds to be tested on (this deactivates parameter n_folds
        and test_size for add_parameters)
        :param save: directory to save tests and results to. If file exists, load previous tests during construction
        :return:
        """

        if compounds_test is not None:
            self.start_test_i = len(compounds)
            self.compounds = compounds + compounds_test
        else:
            self.compounds = compounds
            self.start_test_i = None

        self.save = save

        # list of Tests to run
        self.tests = []
        self.classifiers = []
        # list of results to store
        self.results = []

        self.output_confusion, self.output_process, self.output_result, self.cancel_on_error = None, None, None, None
        self.x, self.y, self.unique_labels, self.unique_features = None, None, None, None
        self.current_feature_type = None
        self.current_noise_train, self.current_transformation_train = {}, {}
        self.current_noise_test, self.current_transformation_test = {}, {}

        # load from save directory
        if save is not None and (os.path.isfile(save) or os.path.isfile("{}.npz".format(save))):
            self.load_state(save)

    def load_state(self, path):
        """
        load state from disk
        :param path: path to the pickle file to load the state from
        :return: nothing
        """
        print("Loading tests and results from disk")
        # default save path
        if path is None:
            path = self.save
        try:
            path_npz = "{}.npz".format(path)
            f = np.load(path_npz)
            tests = f['tests']
            results = f['results']

            # fill new field classifier_input if it is still in the old format, this can be deleted after a time
            convert = False
            if np.shape(tests)[1] == 7:
                print("Conversion: Adding classifier_input to tests array to be able to group after these in analysis "
                      "without having to load/have classifier files then. "
                      "Look white_list at ClassificationTest._classifier_input for what properties are stored")

                try:
                    self._load_classifiers()

                    n_tests = len(f['tests'])
                    n_classifiers = len(self.classifiers)

                    if n_classifiers > n_tests:
                        print("Fix: Too many classifiers, assuming only last classifiers "
                              "were meant since too many were appended")
                        self.classifiers = self.classifiers[(n_classifiers - n_tests):]

                    classifier_input = np.array([ClassificationTest._classifier_input(c) for c in self.classifiers])
                except IOError:
                    print("Conversion failed because classifier file could not be found, "
                          "setting classifier_inputs to empty dicts")
                    classifier_input = [{} for _ in range(len(tests))]
                tests = np.hstack((np.transpose([tests[:, 0]]), np.transpose([classifier_input]), tests[:, 1:]))
                convert = True

            self.tests = [Test(*arg) for arg in tests]
            self.results = [Result(*arg) for arg in results]

            # can be deleted if conversion is no longer required
            if convert:
                self.save_state(ignore_no_data=True)

        except IOError:
            # "backwards compatibility" to pickle files, can be deleted when pickles are gone
            f = open(path, "rb")
            try:
                self.tests = pickle.load(f)
                self.results = pickle.load(f)

                # adapt to new format, move classifiers and store name in tests tuple
                print("Fix: Converting from pickle to npz, hang on... ( .pickle can be deleted afterwards)")
                self.classifiers = [t.classifier_name for t in self.tests]

                tests = np.array(self.tests)
                tests[:, 0] = [t.__class__.__name__ for t in tests[:, 0]]

                # add the classifier_input field
                classifier_input = np.array([ClassificationTest._classifier_input(c) for c in self.classifiers])
                tests = np.hstack((np.transpose([tests[:, 0]]), np.transpose([classifier_input]), tests[:, 1:]))

                self.tests = [Test(*t.tolist()) for t in tests]

                self.save_state(ignore_no_data=True)
            finally:
                f.close()

    def save_state(self, path=None, save_classifiers=True, ignore_no_data=False):
        """
        overwrite file at path with current state
        :param path: path to write state to
        :param save_classifiers: whether to save the classifiers
        :param ignore_no_data: set to True to skip assertion that data needs to be loaded
                (used if conversion processes need to save the state even though no data was loaded)
        :return: nothing
        """
        if not ignore_no_data:
            # not supplying features is essentially interpreted as read-only access to data
            # (Since only useful for graphing results, avoid accidental mutation of data)
            # the ex
            assert not (self.compounds is None and self.x is None)

        # default save path
        if path is None:
            path = self.save

        tests = np.asarray(self.tests)
        results = np.asarray(self.results, dtype=object)
        np.savez(path, tests=tests, results=results)

        if save_classifiers and len(self.classifiers) > 0:
            np.save("{}_classifiers.npy".format(path), self.classifiers)

    def _load_classifiers(self):
        print("Loading classifier objects")
        sys.stdout.flush()
        self.classifiers = np.load("{}_classifiers.npy".format(self.save))
        # remove in future, fix save bug in save interface version
        if len(self.classifiers) > 0 and self.classifiers[0] is None:
            if len(self.classifiers) > 1:
                self.classifiers = self.classifiers[1:]
            else:
                self.classifiers = None
            print("removed None entry at beginning of classifier, everything is fine now...")

    def add_test(self, classifier,
                 n_folds=1, test_size=None, feature_type='absolute',
                 noise=None, noise_test=None, transformation=None, transformation_test=None,
                 description=None, save_immediately=True):

        # default arguments and storing of parameters on test set
        if noise is None:
            noise = {}
        if noise_test is not None:
            noise["noise_test"] = noise_test
        if transformation is None:
            transformation = {}
        if transformation_test is not None:
            transformation["transformation_test"] = transformation_test

        # default behaviour if no compounds_test are supplied
        if n_folds == 1:
            if test_size is None and self.start_test_i is None:
                # test size 0.2 will be assumend if no compounds_test are given
                test_size = 0.2

        # remove noise when there is none (clearer results table and descriptions)
        if 'peak_position' in noise and noise['peak_position'] == 0:
            del noise['peak_position']
        if 'add_random_peaks' in noise and noise['add_random_peaks'] == 0:
            del noise['add_random_peaks']

        # Store Test to be run
        t = Test(classifier.__class__.__name__,
                 ClassificationTest._classifier_input(classifier),
                 n_folds, test_size, feature_type,
                 noise, transformation,
                 description)

        if len(self.classifiers) == 0 and len(self.tests) > 0:
            self._load_classifiers()
        self.classifiers = np.append(self.classifiers, classifier)
        self.tests.append(t)
        self.results.append(ClassificationTest._status_result("pending"))

        if self.save is not None and save_immediately:
            self.save_state()

    def remove_test(self, which):
        """
        removes a test and result with indices which from ram and disk
        :param which: index list specifying which test to remove
        :return:
        """

        del self.tests[which]
        del self.results[which]
        del self.classifiers[which]

        if self.save is not None:
            self.save_state()

    def run(self, use_cache=True, cancel_on_error=True, output_skipped=True, email_interval=None,
            output_confusion=True, output_process=True, output_result=True, intermediate_result=False):
        """
        run the given tests
        :param use_cache: if run_test has been called on some of the tests before, do not reexecute
        :param output_confusion: whether to print confusion matrices during classification
        :param output_process: whether to give status messages during execution
        :param output_result: whether to display result table after being done
        :param output_skipped: whether to display output for tests that are already in cache and are skipped anyways
        :param intermediate_result: output result table after every classifier
        :param cancel_on_error: set true to cancel if test fails to get detailed error information
        :param email_interval: how often to send email updates, interval in hours or "end" to send an email after completion
        :return: void
        """
        assert self.compounds is not None

        self.cancel_on_error = cancel_on_error
        self.output_process = output_process
        self.output_confusion = output_confusion
        self.output_result = output_result

        start = time()
        n = len(self.tests)
        skipped = None
        last_email = start
        for t, r, i in zip(self.tests, self.results, range(n)):
            test_name = ClassificationTest.build_test_name(t, one_line=True)

            if use_cache and r.status == "success":
                if not output_skipped:
                    if skipped is None:
                        skipped = i
                    continue
                print("Test {}/{} skipped because it was already performed ({})".format(i + 1, n, test_name))
                sys.stdout.flush()
            else:
                if not output_skipped and skipped is not None:
                    print("Tests {} to {} skipped because they were already performed".format(skipped, i - 1))
                    skipped = None
                print("Running test {}/{}: {} (started {})".format(i + 1, n, test_name, ctime()))
                sys.stdout.flush()

                if len(self.classifiers) == 0:
                    self._load_classifiers()

                # run the actual test
                try:
                    self.results[i] = self._execute_test(t, self.classifiers[i])
                except MemoryError:
                    # append an empty result
                    self.results[i] = ClassificationTest._status_result("Memory Error")
                    if self.cancel_on_error:
                        raise
                    print("\nERROR: Test '{}'  because of a Memory Error, continuing with next test\n\n".format(
                            test_name))
                    continue
                except ValueError:
                    # append an empty result
                    self.results[i] = ClassificationTest._status_result("Value Error")
                    if self.cancel_on_error:
                        raise
                    print("\nERROR: Test '{}' failed because of a Value Error, continuing with next test\n\n".format(
                            test_name))
                    continue
                except:
                    # append an empty result
                    self.results[i] = ClassificationTest._status_result("failed")
                    if self.cancel_on_error:
                        raise
                    print("\nERROR: Test '{}' failed, continuing with next test\n\n".format(test_name))
                    continue
                finally:
                    # store result persistently
                    if self.save is not None:
                        print("saving tests and results to disk")
                        self.save_state(save_classifiers=False)

                    if email_interval is not None and (time() - last_email) / 3600 > email_interval:
                        try:
                            print("Sending Email with results")
                            self.send_result_email()
                            last_email = time()
                        except:
                            print("Sending update email failed, make sure your credential files is correct "
                                  "and that there is a network connection")

            # display confusion
            if self.output_confusion:
                self.display_confusion(i)

            print("")
            if intermediate_result:
                self.print_result_table(which=slice(i, i + 1))
                print("\n")

        # if last test was skipped, the above skip notice will not call
        if not output_skipped and skipped is not None:
            print("Tests {} to {} skipped because they were already performed".format(skipped, i))

        print("\n")
        if output_result:
            self.print_result_table()

        success = sum([r.status == "success" for r in self.results])
        print("\n\n{}/{} tests completed successfully in {} seconds".format(success, n, time() - start))

        if email_interval is not None:
            try:
                self.send_result_email()
                print("Result Email successfully sent")
            except:
                print("Sending result email failed")

    def _execute_test(self, t, classifier):
        """
        performs an individual test by processing a test t
        :param t: the test namedtuple to process
        :return: the result namedtuple for t
        """
        noise_train = t.noise
        transformation_train = t.transformation

        if "noise_test" in t.noise:
            noise_test = t.noise["noise_test"]
            del noise_train["noise_test"]
        else:
            noise_test = self.current_noise_test

        if "transformation_test" in t.transformation:
            transformation_test = t.transformation["transformation_test"]
            del transformation_train["transformation_test"]
        else:
            transformation_test = self.current_transformation_test

        # make sure noise and feature type is correct for test, recalculate feature space if not
        self._check_feature_space(t.feature_type, noise_train, transformation_train,
                                  noise_test=noise_test,
                                  transformation_test=transformation_test)

        start = time()
        if t.n_folds is not None and t.n_folds > 1:
            k_fold = StratifiedKFold(self.y, n_folds=t.n_folds, shuffle=True)
        elif t.test_size is not None:
            # create split in consistent format with StratifiedKFold for loop
            train, test = [None], [None]
            train[0], test[0] = train_test_split(range(len(self.y)), test_size=t.test_size)
            k_fold = zip(train, test)
        else:
            # train on compounds, test on compounds_test
            train, test = [None], [None]
            train[0] = range(self.start_test_i)
            test[0] = range(self.start_test_i, len(self.compounds))
            k_fold = zip(train, test)

        # prepare output
        correct_rate = np.zeros(t.n_folds)
        n = len(self.unique_labels)
        confusion = np.zeros((t.n_folds, n, n))
        i = 0

        if t.n_folds > 1 and self.output_process:
            print("Working on {} folds: ".format(t.n_folds), end='')

        for train, test in k_fold:

            # perform dimensionality reduction if needed
            if 'dim_reduction' in self.current_transformation_train:
                x_train, x_test = self._reduce_dimensionality(self.x[train], self.x[test])
            else:
                x_train, x_test = self.x[train], self.x[test]

            if self.output_process:
                if t.n_folds > 1:
                    print("{} ".format(i + 1), end='')
                print("fit", end='')

            # fit and predict the classifier
            classifier.fit(x_train, self.y[train])
            if self.output_process:
                print(" predict ", end='')
            predicted = classifier.predict(x_test)

            # store confusion matrix for return
            confusion[i] = confusion_matrix(self.y[test], predicted, labels=self.unique_labels)

            # normalize confusion matrix
            confusion[i] = confusion[i] / np.sum(confusion[i], axis=1)[:, None]
            correct_rate[i] = np.sum(self.y[test] == predicted) / float(len(test))

            i += 1

        # add newline at the end
        print("")

        return Result("success", (time() - start) / float(t.n_folds), correct_rate, confusion)

    def _check_feature_space(self, feature_type, noise_train, transformation_train,
                             noise_test, transformation_test):
        """
        if current noise, feature type is not exactly noise, regenerate x, y, update current noise to match noise
        :param noise: a dict specifying the noise to add
        :param feature_type: what feature space to assert
        :param transformation: what transformation to apply to the feature space
        :return: void
        """
        if self.x is not None and feature_type == self.current_feature_type \
                and cmp(self.current_noise_train, noise_train) == 0 \
                and cmp(self.current_transformation_train, transformation_test) == 0 \
                and (self.start_test_i is None or
                     (cmp(self.current_noise_test, noise_test) == 0 and
                      cmp(self.current_transformation_test, transformation_test) == 0)):

                print("Current noise level, feature_type and transformation is correct, using cached feature space")
                return

        if self.start_test_i is None:
            print("Regenerating feature space {} with noise {} and transformation {}"
                  .format(feature_type, noise_train, transformation_train))
            sys.stdout.flush()

            self.x, self.y, self.unique_features, self.unique_labels = \
                self._generate_feature_space(self.compounds, None,
                                             feature_type,
                                             noise_train=noise_train,
                                             noise_test=noise_test,
                                             transformation_train=transformation_train,
                                             transformation_test=transformation_test)
        else:
            print("Regenerating full feature space {} with noise {},{} and transformation {},{}"
                  .format(feature_type, noise_train, noise_test, transformation_train, transformation_test))
            sys.stdout.flush()

            self.x, self.y, self.unique_features, self.unique_labels = \
                self._generate_feature_space(self.compounds[:self.start_test_i], self.compounds[self.start_test_i:],
                                             feature_type,
                                             noise_train=noise_train,
                                             noise_test=noise_test,
                                             transformation_train=transformation_train,
                                             transformation_test=transformation_test)

        self.current_feature_type = feature_type

        self.current_noise_train = noise_train
        self.current_transformation_train = transformation_train
        self.current_noise_test = noise_test
        self.current_transformation_test = transformation_test

    def print_result_table(self, *args, **kwargs):
        print(self.result_table(*args, **kwargs))

    def result_table(self, table_format='normal', which=None, include_header=True, columns=None):
        """
        output a table of all classification results to the console
        :param table_format: see tabulate documentation for options
        :param which: which tests to display MUST BE A SLICE (use slice(begin, end))
        :param include_header: whether to add header line or not
        :param columns: array of columns (by index) to return, None for all
        :return: the result table as string
        """
        if which is None:
            tests = self.tests
            results = self.results
            offset = 0
        else:
            assert isinstance(which, slice)
            tests = self.tests[which]
            results = self.results[which]
            offset = which.start

        headers = ["#", "Classifier", "noise", "feature transformation", "folds",
                   "t/fold [s]", "correct", "sigma"]
        output = np.empty((len(tests), len(headers)), dtype=object)

        for i, t, r in zip(range(len(tests)), tests, results):

            output[i, 0] = i + offset
            inputs = [t.classifier_input[field] for field in t.classifier_input.keys()]
            output[i, 1] = "{} ({})".format(t.classifier_name, ''.join('%s ' % i for i in inputs))

            if "peak_position" in t.noise:
                output[i, 2] = '%.2e' % t.noise["peak_position"]
            else:
                output[i, 2] = "0"
            output[i, 2] += " / "
            if "add_random_peaks" in t.noise:
                output[i, 2] += str(t.noise["add_random_peaks"])
            else:
                output[i, 2] += "0"

            output[i, 3] = t.transformation
            if t.n_folds == 1:
                output[i, 4] = t.test_size
            else:
                output[i, 4] = t.n_folds
            output[i, 5:] = r.status

            if r.status == "success":
                output[i, 5] = round(r.run_time, 2)
                output[i, 6] = round(np.mean(r.correct_rate) * 100, 2)
                output[i, 7] = round(np.std(r.correct_rate) * 100, 2)

        if columns is not None:
            output = output[:, columns]
            # built in lists do not support array indexing so use this
            headers = np.array(headers)[columns].tolist()

        if include_header:
            return tabulate(output, headers, tablefmt=table_format)
        else:
            return tabulate(output, tablefmt=table_format)

    def display_confusion(self, which=None, display_as='img', unique_labels=None, to_file=False):
        """
        output confusion matrix to console, for cross validations displays mean of a test
        :param which: index specifying the confusion matrix of what test to display
        :param display_as: whether to display the matrix as table or image
        :param unique_labels: supply unique labels explicitly in case no data is currently loaded
        :param to_file: whether to output confusion matrix to file (only valid for img)
        :return: void
        """
        if which is None:
            # output all matrices
            to_display = self.results
            to_display_test = self.tests
        else:
            to_display = [self.results[which]]
            to_display_test = [self.tests[which]]

        for t, r in zip(to_display_test, to_display):
            # display error if classifier has not completed successfully yet
            if not r.status == 'success':
                print("Cannot display confusion matrix when the classifier has not completed successfully")
                break

            confusion = r.confusion
            if unique_labels is None:
                labels = self.unique_labels
            else:
                labels = unique_labels

            if labels is None:
                print("Cannot display confusion matrix without feature space generated(need labels) "
                      "Call _generate_feature_space if you need the confusion matrix now "
                      "or pass unique_labels with the function call")
                break

            if np.size(np.shape(confusion)) == 3:
                confusion = np.mean(confusion, 0)

            if display_as == 'table':
                # add row headers
                confusion = np.hstack((labels.reshape(np.size(labels), 1), confusion))
                print(tabulate(confusion, np.insert(labels, 0, "Truth\Pred.").tolist()))
            elif display_as == 'img':
                title = "Confusion matrix of {}".format(ClassificationTest.build_test_name(t))

                plt.imshow(confusion, interpolation='nearest')
                plt.title(title)
                plt.colorbar()
                ticks = np.arange(len(labels))
                plt.xticks(ticks, labels, rotation=90)
                plt.yticks(ticks, labels)
                plt.ylabel('True Label')
                plt.xlabel('Predicted')
                if to_file:
                    plt.savefig('graphs/confusion.pdf',  bbox_inches="tight", dpi=300)
                plt.show()

    @staticmethod
    def _generate_feature_space(compounds_train, compounds_test=None,
                                feature_type='absolute',
                                noise_train=None, transformation_train=None,
                                noise_test=None, transformation_test=None):
        """
        wrap feature generation for extendability
        :param compounds: list of compounds namedtuple
        :param feature_type: what kind of features to extract from compounds
        :param noise: dict describing noise to be added
        :param transformation: dict describing transformation to be applied to feature space
        :return: the feature space x
        """
        start = time()

        if noise_train is None:
            noise_train = {}
        if noise_test is None:
            noise_test = {}
        if transformation_train is None:
            transformation_train = {}
        if transformation_test is None:
            transformation_test = {}

        # perform deep copy to avoid modifying original compound list
        print("Adding noise {} and transformation {} to the feature space"
              .format(noise_train, transformation_train))

        noisy_compounds = ClassificationTest. \
            _make_noisy_compounds(compounds_train, noise_train, transformation_train)

        if compounds_test is not None:
            # perform deep copy to avoid modifying original compound list
            print("Adding noise {} and transformation {} to the test feature space"
                  .format(noise_test, transformation_test))

            start_i = len(noisy_compounds)

            noisy_compounds += ClassificationTest. \
                _make_noisy_compounds(compounds_test, noise_test, transformation_test)

            if "match_peaks" in transformation_test and transformation_test["match_peaks"]:
                print("Matching peaks")
                features = [m for c in noisy_compounds[:start_i] for m in c.mzs]

                for c in noisy_compounds[start_i:]:
                    for i in range(len(c.mzs)):
                        c.mzs[i] = features[(np.abs(np.array(features) - c.mzs[i])).argmin()]

        if feature_type == 'absolute':
            x, y, unique_features, unique_labels = ml.fs_absolute(noisy_compounds)
        else:
            raise NameError('feature generation method unknown')

        print("Feature space generated in {} seconds".format(time() - start))

        assert len(y) == np.shape(x)[0]

        return x, y, unique_features, unique_labels

    @staticmethod
    def _make_noisy_compounds(compounds, noise, transformation):
        """
        transform compounds namedtuple array by adding noise
        :param compounds:
        :param noise:
        :param transformation:
        :return:
        """
        """
        # skip copy when nothing to be done
        if (noise == {} or ("peak position" in noise and noise['peak_position'] == 0 and
                                    "add_random_peaks" in noise and noise["add_random_peaks"] == 0)) and \
                (transformation == {} or ("round" in transformation and transformation["round"] is None)):
            return compounds
        """
        noisy_compounds = deepcopy(compounds)

        # add noise
        if 'peak_position' in noise:
            for c in noisy_compounds:
                for i in range(len(c.mzs)):
                    c.mzs[i] += np.random.normal(0, noise['peak_position'])

        if 'add_random_peaks' in noise:
            # draw from poisson distribution how many peaks to remove of a compound
            add_no_peaks = np.random.poisson(noise['add_random_peaks'], len(noisy_compounds))

            for n, c in zip(add_no_peaks, noisy_compounds):
                to_add = np.random.uniform(0, 1000, n)
                # workaround since namedtuples attributes are immutable
                for i in to_add:
                    c.mzs.append(i)

        if 'remove_peaks' in noise:
            raise NameError('removing peaks not implemented')

        # round features if necessary
        if "round" in transformation and transformation["round"] is not None:
            for c in noisy_compounds:
                # workaround since namedtuples are immutable, but lists are not
                for i in range(len(c.mzs)):
                    c.mzs[i] = round(c.mzs[i], transformation['round'])

        return noisy_compounds

    def _reduce_dimensionality(self, x_train, x_test):
        """
        reduce the dimensionality in the feature set. Since this can (sensibly) only be done after the train/test split,
         this code is not included in generate_feature space
        :param x_train: feature matrix of training data
        :param x_test: feature matrix of test data
        """
        print("Performing dimensionality reduction on feature space")
        sys.stdout.flush()
        start = time()

        reduction = self.current_transformation_train['dim_reduction']
        if reduction == 'LowVariance':
            from sklearn.feature_selection import VarianceThreshold

            t = self.current_transformation_train['threshold']
            reducer = VarianceThreshold(threshold=t)
        elif reduction == 'Univariate':
            from sklearn.feature_selection import SelectKBest
            from sklearn.feature_selection import chi2

            k = self.current_transformation_train['k']
            reducer = SelectKBest(chi2, k=k)
        elif reduction == 'RandomProjection':
            from sklearn.random_projection import SparseRandomProjection

            eps = self.current_transformation_train['eps']
            if 'n_components' in self.current_transformation_train:
                n_components = self.current_transformation_train['n_components']
            else:
                n_components = 'auto'

            reducer = SparseRandomProjection(n_components=n_components, eps=eps)
        else:
            # remove transformation from dict since not performed
            del (self.current_transformation_train['dim_reduction'])

            raise NameError("The required dimensionality reduction is unknown, skipping dimensionality reduction")

        # transform the data
        x_train = reducer.fit_transform(x_train)
        x_test = reducer.transform(x_test)

        print("Dimensionality reduction took {} s".format(time() - start))

        return x_train, x_test

    def clear(self):
        """
        remove all tests and results
        :return:
        """
        self.tests = []
        self.results = []
        self.classifier = []

    def clear_cache(self, which=None):
        """
        clears all results
        :param which: which result to clear, if none given, clear all
        :return:
        """
        if which is not None:
            self.results[which] = ClassificationTest._status_result("pending")
        else:
            self.results = [ClassificationTest._status_result("pending") for _ in self.results]

    def send_result_email(self, credentials="credentials"):
        """
        send an email about the progress of the tests
        :param credentials: file with server, username and password in 3 lines
        :return:
        """
        import smtplib

        f = open(credentials, 'r')
        server = f.readline().rstrip()
        user = f.readline().rstrip()
        pw = f.readline().rstrip()
        f.close()

        completed = sum([r.status == "success" for r in self.results])
        pending = sum([r.status == "pending" for r in self.results])
        failed = len(self.results) - completed - pending

        status_str = "{}/{} Tests completed successfully, {} Tests failed, {} Tests pending" \
            .format(completed, len(self.results), failed, pending)

        if pending == 0:
            subject = "Subject: ClassificationTest: Test {} completed".format(self.save)
        else:
            subject = "Subject: ClassificationTest: Status update of test {}".format(self.save)

        msg = "\r\n".join([
            "From:" + user,
            "To:" + user,
            subject,
            "",
            status_str,
            self.result_table()
        ])

        server = smtplib.SMTP(server)
        server.ehlo()
        server.starttls()
        server.login(user, pw)

        server.sendmail(user, user, msg)

    @staticmethod
    def _classifier_input(classifier):
        """
        whitelist the classifier properties to store
        (need whitelist cause otherwise very many properties are stored)
        given a classifier, return dict to store in classifier_input
        :param classifier: the classifier to process
        :return: the dict
        """
        white_list = ["alpha", "n_iter", "penalty", "loss", "dual", "solver", "tol", "n_estimators"]
        inputs = {}
        for key, val in classifier.__dict__.iteritems():
            if key not in white_list:
                continue
            inputs[key] = val
        return inputs

    @staticmethod
    def build_test_name(t, one_line=False):
        """
        parse test name tuple into descriptive string
        :param t: a namedtuple of type test
        :param one_line: set to true to output test name in just 1 line
        :return: the output string
        """

        string = "Classifier {}".format(t.classifier_name)

        if t.n_folds > 1:
            string += " with {}-fold cross validation".format(t.n_folds)
        else:
            if t.test_size is None:
                string += " with real data as test"
            else:
                string += " with test size of {}".format(t.test_size)

        string += ", \nfeature selection '{}'".format(t.feature_type)

        if bool(t.noise):
            if t.noise.get("peak_position") is not None:
                string += " and gaussian noise on peaks (sigma={})" \
                    .format(t.noise["peak_position"])

            if t.noise.get("add_random_peaks") is not None:
                string += ", added {} random peaks" \
                    .format(t.noise["add_random_peaks"])

            if t.noise.get("remove_peaks") is not None:
                string += ", removed {} peaks in data" \
                    .format(t.noise["remove_peaks"])
        else:
            string += ", no artificial noise"

        if bool(t.transformation):
            string += " \nand transformation {}".format(t.transformation)

        if one_line:
            string = string.replace("\n", "")

        return string

    @staticmethod
    def _status_result(status):
        """
        builds a result namedtuple that only contains the given status
        :param status: string for status variable
        :return: the Result namedtuple
        """
        fill_up = [None for _ in range(len(Result._fields) - 1)]
        return Result(status, *fill_up)
