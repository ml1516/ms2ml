\documentclass[main.tex]{subfiles}

\begin{document}
\section{Theory}
\subsection{Mass Spectrometry}
\label{mass_spectrometry}

Mass spectrometry is an analytical method to determine the mass of each molecule present in a sample, together with its abundance. It has become an important tool in many disciplines to gain insight into the chemical composition of a sample. The data produced by a mass spectrometer is called a mass spectrum and displays how often each molecular mass was found by the instrument.

A mass spectrometry experiment can be divided into three sub-steps:
\begin{enumerate}
    \item Ionization: The requirement for analysis by a mass spectrometer is the ability to create ions. MALDI, one of the most common ionization techniques for biomolecules, works by surrounding the sample with small organic molecules which are able to absorb a laser beam that triggers the ionization. 
    \item Mass selection: During the second phase, the ions created in the first step are separated by their mass-to-charge ratio ($m/z$).
    \item Detection: The third step is the actual detection of the arriving ions. By recording the induced charge of arriving ions, the mass spectrum is produced.
\end{enumerate}

\paragraph{Tandem Mass Spectrometry}
The information gained from regular mass spectrometry is often not sufficient to infer the required amount of chemical knowledge. This is mainly because the relation between molecules and molecular masses is a one-to-many relationship: Each molecule has a unique mass but one specific mass can belong to many molecules. The process of labelling mass spectrometry data with molecules is called \textbf{molecular annotation}.

One approach to aiding molecular annotation is tandem mass spectrometry (also known as MS/MS or MS\textsuperscript{2}). It consists of two sequential mass spectrometery measurements, linked by a fragmentation process: After the first mass spectrometer has separated the ions by their mass, they are iteratively dissociated into smaller particles, such as functional groups and sidechains. The dissociation is usually induced by colliding the ion with a neutral gas. The masses of the particles that result from the collision are then measured by the second mass spectrometer. That is, for each previously known molecular mass from the first step, one now has a new mass spectrum displaying which part of the molecule was found how often. So, the obtained spectrum resembles the probability distribution of fragments being formed during the collision. Such a spectrum is called a \textbf{fragmentation spectrum}. The outcome depends on the chemical properties of the respective molecule where chemically similar molecules will also have similar fragmentation spectra. Therefore, it reveals valuable information about the chemical structure of the compound allowing for more profound reasoning about the identity of the observed molecule.

% TODO: Explain molecular annotaion with MS2: similarity search, etc.

\subsection{Chemical databases}
\label{databases}
Chemical knowledge is organized in different kinds of databases, each specialized into certain types of information. Broadly speaking, there are two major database kinds that are of interest to us. \textbf{Molecular databases} usually provide rich meta information about molecules, such as sum formula, structure, chemical and physical properties, etc. On the other hand, \textbf{spectral databases} specifically provide information about the molecule's fragmentation, most importantly its fragmentation spectrum.

Acquiring high-quality fragmentation spectra experimentally involves careful measurement and curation which makes it a tedious process. Furthermore, many compounds cannot be easily synthesized in a pure enough manner. As a result, most spectral databases are significantly smaller than molecular databases. This immediately impacts their ability to serve for molecular annotation since there is a tradeoff between the level of detail and the quantity of the data. The latter is especially important for molecular annotation since the above descibed similarity-matching methods require the database to contain the substance that is being searched for.

\paragraph{LipidBlast}
\label{lipidblast}
To overcome the need for manually selected fragmentation spectra, there have been efforts to generate them computationally (so called \textit{in-silico} methods) \cite{lipidblast} \cite{fahfa} \cite{isis}. We picked one of these synthetic databases as the source for our training data, namely LipidBlast. The main rationale behind this choice are:
\begin{itemize}
    \item Lipids are a class of molecules for which there exists a broadly accepted hierarchical structure into which they can be ordered (see section \ref{lipids}) \cite{lipidmaps}.
    \item The spectra are all labeled with the respective molecule's position in the hierarchy. We therefore avoid the need to link LipidBlast to a molecular database for retrieving the required meta information.
    \item The spectrum generation was consistently performed on a well-defined set of rules which we expected to have a positive impact on classifier performance.
\end{itemize}

LipidBlast uses a subset of the substances from LIPID MAPS\footnote{\url{http://www.lipidmaps.org/} supplies a database (called LMSD) of classified fragmentation spectra on lipids created in an effort to understand lipid metabolism better} to create their database. They simulate a fragmentation process based on both theoretical rules and experimental heuristics. They exploit knowledge about the chemical reactions in which each lipid class is involved, respectively.

Despite LipidBlast's automated approach and its focussing on lipids, it is far from being as complete as molecular databases. It only featuring molecules from only 29 out of 354 existing lipid classes. Compared to the 88 836 364 substances included in the molecular database PubChem \footnote{\url{https://pubchem.ncbi.nlm.nih.gov/}}, the 119 341 compounds from LipidBlast seem few. However considering that usual in-house spectral libraries of research groups only contain a couple of hundred molecules, LipidBlast is a very good choice for lipid data.

We chose a machine learning approach to molecular annotation using LipidBlast as training data, aiming to assign the correct class label to experimentally acquired spectra. We achieve this by labelling each spectrum from LipidBlast with the chemical meta information from LIPID MAPS and predicting this label for every unseen spectrum.

\subsection{Lipids}
\label{lipids}
Lipids are a class of molecules with a specified structure. They consist of a constant part, common to all lipids, and a variable part that makes them individual. Figure \ref{fig:lipid_conceptual} displays the general structure of a lipid. The black-colored part is called the glycerol backbone. The colored parts, $R_1$, $R_2$ and $R_3$ make a lipid unique. They are called the sidechains and they consist of fatty acids. A lipid can have less than three sidechains but requires at least one. A lipid is classified into a category based on the number and type of its sidechains. The LIPID MAPS classification system \cite{lipid_classification} is a hierarchical set of categories into with lipids are ordered based on their sidechains. Table \ref{tbl:level_examples} shows an overview of the hierarchy. For example, the lipid species is determined by the number of carbon atoms and the number of double bonds in all of the side chains. The structural subspecies additionaly disciminates lipids by the distribution of carbon atoms across the sidechains.

For our classification problem we chose the \emph{Sub Class} level since this is the most detailed level for which LipidBlast still provides a reasonable amount of molecules per class, the smallest class being of size $33$. The complete list of sub classes that occur in LipidBlast as well as their abundances can be found in appendix \ref{class_abbreviations}.

\begin{figure}[hbt]
    % \vspace*{0.2in}
    \centering
    \includegraphics[width=0.4\textwidth]{img/lipid_conceptual}
    \caption{Conceptual figure of a lipid. The black-colored part is the glycerol backbone. The sidechains are denoted by $R_1$, $R_2$ and $R_3$. \textit{Source: \url{http://chemistry.about.com/od/lecturenoteslabs/a/lipids-introduction.htm} (public domain)}}
    \label{fig:lipid_conceptual}
\end{figure}

\begin{table}[hbt]
\begin{tabular}{ll}
    Level                 & Example \\
    \hline
    Category              & Glycerophospholipid \\
    Main Class            & Glycerophosphocholine \\
    Sub Class             & Monoalkylmonoacylglycerophosphocholine \\
    Species               & PC(O-36:5) \\
    Molecular subspecies  & PC(O-16:1\_20:4) \\
    Structural subspecies & PC(P-16:0/20:4) \\
    Isomeric subspecies   & PC(P-16:0/20:4(5Z,8Z,11Z,14Z)) \\
    \hline
\end{tabular}
\caption{The levels of the LIPID MAPS classification hierarchy, each with an example. The levels are ordered by their degree of detail. \emph{Categeory} is the most general level, \emph{Isomeric subspecies} is the most detailed one. \cite{swisslipids}}
\label{tbl:level_examples}
\end{table}

\end{document}
