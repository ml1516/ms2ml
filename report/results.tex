\documentclass[main.tex]{subfiles}

\begin{document}
\section{Results}

\subsection{Dimensionality reduction}
\label{dim_reduction_results}

In the following, the results for data visualization with dimensionality reduction and differences in runtime and precision between classification with and without dimensionality reduction are described.

Regarding data visualization: By reducing the dimensionality of the data to two dimensions it can be plotted on a 2D graph. This enables a visual survey of the data, and can indicate whether significant structure exists within the data. The data points are colour-coded according to their lipid class to asses whether the structure visible corresponds to the known chemistry. Since the meaning of the two resulting dimensions is hard to derive after reducing them from more than 40000 dimensions, the axes don't have any particular meaning/description. 

\paragraph{PCA} Figure \ref{fig:plot_pca} shows the embedding which resulted from using PCA for dimensionality reduction for sampled data (using the whole data set would result in problems with respect to runtime and computer memory). Through the orthogonal linear transformation of the compounds to this new coordinate system linear combinations of the different features were found, resulting in the visualization of three clusters: one widely spread cluster in the top part of the plot and two small clusters in the bottom part of the plot. 

Both components are useful for describing the underlying relationship between the compounds. One relationship is that the variance regarding the second principal component reduces with increased values of the second principle component \footnote{Since the compounds are spread more along the horizontal axis, the first principal component is represented by the x-axis. In comparison, along the vertical axis the compounds are spread along a part in the top of the plot from 0 to 0.5 and a part in the bottom of the plot from -1.5 to -0.5 while showing nearly no variance between -0.5 to 0.
}. Furthermore it is visible that mostly CL and Ac4PIM2 are lying next to each other between -0.5 to 0 along the first principal component while the other compounds are clustered next to each other from 0 to 0.5. This might indicate that the first principal component assigns compounds with respect to unsaturated and saturated lipds. Another important observation is that one of spectrum-peaks can be used to seperate most of the compounds in two clusters (the top cluster and the bottom two clusters).

\begin{figure}[h!]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{img/embedding_compounds/1_pca.png}}
\caption{Dimensionality Reduction with PCA for 851 samples: Reducing 3831 features to 2 features/components}
\label{fig:plot_pca}
\end{figure}

\paragraph{LLE} Figure \ref{fig:plot_lle} shows the embedding which resulted from using LLE for dimensionality reduction with K = 100 as the number of neighbours, which gives an optimal interpretability of the data - compared to other amounts of neighbours such as 10, 50, 150 and 200. Through the structured appearance of this plot it is visible that by means of the general principle of manifold learning, the 3831-dimensional data is ``unfold'' while preserving the neighbourhood structure of nearby components.

This unfolding results in three regions which are seperated by the compounds regarding the two dimensions. While the compounds CL, Ac4PIM2, Cer-d and Ac3PIM2 are distributed mostly across the whole region with a tendency towards the linear bounds (e.g., (-0.15, -0.05) to (0, 0)) and the middle (around (0,0)) the other compounds lie generally on the bounds or in the middle. 

In comparison to PCA, LLE keeps the low-dimensional representations - which can be derived from high-dimensional data that lies on or near a low-dimensional, non-linear manifold - of very similar datapoints close together \cite{tsne}. Since this is not possible with a linear mapping, the usage of LLE can result in a clearer structure than that of PCA, which is visible in this plot. 

\begin{figure}[h!]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{img/embedding_compounds/2_lle.png}}
\caption{Dimensionality Reduction with LLE for 851 samples and 100 neighbours: Reducing 3831 features to 2 features (components)}
\label{fig:plot_lle}
\end{figure}

\paragraph{tSNE} Non-linear dimensionality reduction techniques that aim to preserve the local structure of data like LLE are often note very successful at visualizing high-dimensional data \cite{tsne}. Therefore, also tSNE was used to visualize the relationships between the compounds. 

Figure \ref{fig:plot_tsne} shows the embedding which resulted from using first PCA to reduce the dimensions of the sampled data from 3831 dimensions to 50 dimensions and then using tSNE for reducing the remaining dimensions to two dimensions and embedding the compounds with respect to them (in case of tSNE it is recommended to reduce the number of dimensions to 50 if the number of features is very high; therefore PCA was used first \cite{tsne_sklearn}). Since the structure of the data increased in comparison to LLE, this plot indicates that much of the local structure of the compounds was captured, while also revealing the presence of clusters at several scales. 

The appearances of these clusters are very clear and geometrically well shaped, making it easy to distinguish one from another - e.g., the clusters around the point (-30, -45) or (10, -35). Furthermore, these clusters also contain generally the same kind of compounds. For example, the cluster around (-30, -45) mostly contains the compounds PS and SM; the cluster around (10, -35) mostly contains TG and [glycan]-Cer. 

Another more interesting observation is that the compounds CL, Ac4PIM2 and Cer-d are distributed without any particular structure along the plot again like in the previous cases (see Figure \ref{fig:plot_pca} and Figure \ref{fig:plot_lle}). This might indicate that the classification for all compounds except for CL, Ac4PIM2 and Cer-d might lead to better overall generalization accuracies if trained without them, because of the underlying structure behind them. 

The results regarding visualization of the data can be summarized as: 
\begin{enumerate}
    \item Without any domain-specific knowledge visual interpretations that might help explaining missclassifications in \cref{fig:confusion:no_noise} or \cref{fig:confusion:tree} could not be derived.
    \item The process of feature extraction appears to be succesful, because a lower-dimensional structure can be derived from them.
    \item The classification task should lead to acceptable results because the plots indicate that the data has a particular structure. 
\end{enumerate}

\begin{figure}[h!]
\centering
\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{img/embedding_compounds/3_tSNE.png}}
\caption{Dimensionality Reduction with PCA and tSNE for 851 samples: Reducing 3831 features with PCA to 50 features and then with tSNE to 2 features}
\label{fig:plot_tsne}
\end{figure}

Regarding the application of dimensionality reduction in order to increase the precision or decrease the runtime of the classifiers no significant improvements were achieved for any of the classifiers. The precision for all classifiers with dimensionality reduction lied below those without dimensionality reduction. Therefore, only the runtimes for LinearSVC (based on the library libsvm, uses a radial basis function kernel by default) and decision trees were worth reducing, see \cref{tbl:no_noise}. The runtime of LinearSVC even increased slightly after applying variance threshold and univariate feature selection. In comparison, the runtime of decision trees decreased significantly from 6324.29 seconds to 489.18 seconds (\cref{tbl:dr_dt}) without decreasing the precision too much (94.16 vs. 91.97) after applying variance threshold (threshold$=0.00002$) and also after applying univariate feature selection to 141.37 seconds with a slight decrease of precision to 91.21.

\begin{table}[h]

\begin{tabular}{rllll}
\hline
   \# &Dimensionality Reduction Method & runtime [s]   & correct      \\
\hline
   0 & No dimensionality reduction                  & 6324.29 & 94.16   \\
   1 & Variance threshold (threshold$=0.00002$)     & 489.18  & 91.97   \\
   2 & Variance threshold (threshold$=0.00005$)     & 168.09  & 91.78   \\
   3 & Variance threshold (threshold$=0.00008$)     & 68.68   & 88.56   \\
   4 & Variance threshold (threshold$=0.00027$)     & 28.25   & 81.83   \\
   5 & Variance threshold (threshold$=0.00064$)     & 13.17   & 70.02   \\
   6 & Variance threshold (threshold$=0.00125$)     & 7.23    & 56.34   \\
   7 & Univariate feature selection ($k=8000$)      & 29.50   & 78.06   \\
   8 & Univariate feature selection ($k=16000$)     & 60.59   & 85.68   \\
   9 & Univariate feature selection ($k=24000$)     & 86.52   & 87.15   \\
   10 & Univariate feature selection ($k=32000$)    & 119.54  & 90.71   \\
   11 & Univariate feature selection ($k=40000$)    & 141.37  & 91.21   \\
   
\hline

\end{tabular}

\caption{Performance of decision trees on pure LipidBlast data without any noise and with/without dimensionality reduction. The classification task 0 was run using no dimensionality reduction, 1 to 6 with variance threshold and 7 to 11 with univariate feature selection. See sklearn\cite{sklearn} for parameter definitions.}
\label{tbl:dr_dt}
\end{table}

\subsection{Noise-free data from LipidBlast}
\label{no_noise}
In a first step, some of the classification algorithms that sklearn provides were tested on the pure, parsed LipidBlast data without any noise added on peak positions or added random peaks by using the absolute peaks feature extraction. (see \cref{extraction}).

\begin{table}[h]

\begin{tabular}{rllll}
\hline
   \# & Classifier                                 & t/fold [s]   & correct     & $\sigma$       \\
\hline
   3 & RidgeClassifier 1 ($\alpha=1$)         & 36.48        & 97.13       & 0.03        \\
   4 & RidgeClassifier 2 ($\alpha=1$)          & 18.44        & 97.09       & 0.1         \\
   5 & RidgeClassifier 3 ($\alpha=1$)         & 26.08        & 97.1        & 0.04        \\
   6 & RidgeClassifier 4 ($\alpha=0.01$)        & 66.27        & 96.99       & 0.06        \\
   7 & RidgeClassifier 5 ($\alpha=0.05$)        & 61.09        & 97.01       & 0.05        \\
   8 & RidgeClassifier 6 ($\alpha=0.1$)         & 55.88        & 96.99       & 0.04        \\
   9 & RidgeClassifier 7 ($\alpha=0.5$)         & 43.22        & 97.06       & 0.05        \\
  13 & LinearSVC 4 ($solver='l1'$) & 32.9         & 97.04       & 0.05        \\
  14 & LinearSVC 5 ($solver='l2'$) & 140.25       & 96.98       & 0.08        \\
  10 & Perceptron 1 ($n\_iter=5$)     & 4.91         & 96.75       & 0.1         \\
  11 & Perceptron 2 ($n\_iter=15$)   & 13.01        & 96.82       & 0.09        \\
  12 & Perceptron 3 ($n\_iter=50$)   & 40.57        & 96.7        & 0.06        \\
  18 & ExtraTreesClassifier   &       666.26 &     95.45 &       0 \\
  17 & RandomForestClassifier &       818.97 &     95.2  &       0 \\
  16 & DecisionTreeClassifier    &      5932.79 &     95.1  &       0 \\
   0 & BernoulliNB 1 ($\alpha=0.01$)                        & 0.86         & 94.9        & 0.07        \\
   1 & BernoulliNB 2 ($\alpha=0.017$)                      & 0.86         & 94.91       & 0.12        \\
   2 & BernoulliNB 3 ($\alpha=0.025$)                      & 0.87         & 94.92       & 0.06        \\
  15 & SVC                 &      5995.68 &     33.72 &       0 \\    
\hline

\end{tabular}

\caption{Performance of different classifiers on pure LipidBlast data without any noise. Classifiers 0-14 were run using 5-fold cross validation, 15-18 with a test size of 20\%. See sklearn\cite{sklearn} for classifier and parameter definitions. (Default parameters and some parameters such as solution tolerance not included here for legibility)}
\label{tbl:no_noise}
\end{table}

In \cref{tbl:no_noise} the results of this first test are displayed. Please note that classifiers 0-14 were run on five-fold cross validation, the others with a test size of 20\%. This was done for performance reasons since these classifiers took up to three or four orders of magnitude longer than the fastest classifier, which is the Naive Bayes classifier in an implementation optimized for binary, sparse data. This classifier also already yields a correct classification rate of 95\%. Using Ridge classifiers, Perceptrons and support vector machines with a Linear kernel (LinearSVC) the rate rises another 2\%. These two classifiers cannot be distinguished by their classification rate given the uncertainty estimated from the standard deviation $\sigma$ of the cross validation. They do however take longer, with the Perceptrons taking 5 to 40, the Ridge classifiers 30 to 70 times longer compared to the Naive Bayes classifier.

The support vector machines in the SVC implementation which is based on the library libsvm and by default uses a radial basis function kernel performs poorly both in classification rate and in execution time.

Trees have a similiar classification rate as the naive Bayes classifier, but since their execution time was far too long it was decided that only very few tests of them would be run on the noisy data to check whether the classification rate is still comparable to other classifiers with noise added.
What seems strange within the execution times of the tree classifiers is that the decision tree classifier takes much longer to execute than the random forest classifier while the sklearn documentation clearly states that decision trees are used for the random forest (ten of them). This is probably due to the fact that while the default decision tree uses all n features for the optimal split, random forests only considers $\sqrt(n)$, leading to a better performance in high-dimensional feature spaces.

\begin{figure}[hbt]

\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{img/confusion_no_noise.pdf}}
\caption{Example confusion matrix without any noise}
\label{fig:confusion:no_noise}
\end{figure}

When looking at the corresponding confusion matrices of the tests above, there are very little differences between the tests. \cref{fig:confusion:no_noise} is the confusion matrix of the Naive Bayes classifier, but the observations apply to all classifiers. The confusion matrix is normalized such that the sum over every row is one.

Most classes can apparently be classified very well with a very low error rate. There are two exceptions to this to point out: The classes lyso-PE and lyso-PC are commonly confused with each other, but usually ($>70\%$) classified correctly. The other pair of classes commonly confused is DGDG and MGDG. DGDG is classified as MGDG in almost exactly 50\% of the cases while MGDG has a 20\% chance of being misclassified as DGDG. The reason for this lies in the very high similarity between the chemical structure of these four compounds which can be seen in Appendix \cref{chem_structures}. 

The only difference between MGDG and DGDG is a missing Glactosyl-group. Since the compound breaks up into sub-compounds during the measurement process, a DGDG contains all features an MGDG does, a DGDG-spectrum therefore will very easily be missclassified by matching to an MGDG-spectrum which may just contains as little as one additional peak.

Between lysoPC and lysoPE there also is a very high chemical similarity , explaining the frequent confusion of the two of them.

Tree classifiers, on the other side, tend to have a confusion matrix as displayed in \cref{fig:confusion:tree}. Since no cross validation as in \cref{fig:confusion:no_noise} was performed, the entire matrix is noisier, but it is interesting to see how the two pairs of misclassified compounds are not confused as often as it happens with other classifiers. This could indicate overfitting to some of the features that separates these pairs in the simulated data.

\begin{figure}[hbt]
\makebox[\textwidth][c]{\includegraphics[width=1.2\textwidth]{img/confusion_tree.pdf}}
\caption{Confusion matrix of an example tree classifier. The DGDG/MGDG and lysoPC/lysoPE confusion is much smaller}
\label{fig:confusion:tree}
\end{figure}


\subsection{Noisy LipidBlast data}
\label{noise_results}
The noise was added as described in \cref{noise} and hundreds of different combinations of the two chosen noise parameters and rounding levels were used to classify the data using different classifiers. Since the computing power for this project was relatively limited, slow classifiers could only be tested in a very limited set of scenarios and only the Naive Bayes classifier was tested using ten-fold cross validation, all other classifiers were tested in a single split with a test set size of 20\,\%. This also at least partly explains the smoothness of all tests run with the Naive Bayes classifier compared to the other classifiers.

Within the diagrams, a red vertical line was inserted to indicate the theoretical noise level that is to be expected as well as a blue vertical line to indicate the level of rounding that was performed.

To compare different classifiers directly with each other, \cref{fig:noise:classifiers} is a general comparison of classifier precision.

\begin{figure}[hbt]

\makebox[\textwidth][c]{\includegraphics[width=1.4\textwidth]{img/noise/Classifiers_3_added_peaks_0.pdf}}
\caption{Comparison of classification performance between classifiers when no random peaks are added. (Single execution with test size of 20\%)}
\label{fig:noise:classifiers}
\end{figure}

As expected, the classification rate of all classifiers drops as the simulated experimental accuracy (noise) gets larger. The Naive Bayes classifier seems to be influenced by the noise the least since for high noise levels it becomes the classifier with the highest prediction rate.

Forest-based classifiers however continue to disappoint. Not only does the execution time rise even further with added noise, ocassionally taking 6h to perform a single fold (which is the reason why only one fold at very few noise levels was performed for these classifiers), but at the same time the correct classification rate drops the fastest, making forests the worst classifier choice for this problem.

Perceptrons also have a high classification rates, but there is a high variance in the results. This however may be due to the fact that only the Naive Bayes classifier was run using cross validation, the perceptron's classification rate curve would probably be slightly smoother if it had also been run using cross validation.

Ridge classifiers do maintain their high prediction rates way over the expected noise level, at very high noise levels the Naive Bayes classifier seems to perform equally well. Since the Naive Bayes classifier is multiple orders of magnitude faster than any other classifier, it is a great choice for testing. For the following diagrams it was therefore selected as an example classifier.

\begin{figure}[hbt]
\centering
\includegraphics[width=\textwidth]{img/noise/BernoulliNB_1_rounding_None.pdf}
\caption{Classification rate of the Bernoulli Naive Bayes classifier in a noisy feature space when not rounding. Note: scale of the x-axis chosen so that it is equal for all diagrams}
\label{fig:noise:bnb:no_rounding}
\end{figure}

We first investigated whether our naive feature vector generation was suitable for noisy data. However, \cref{fig:noise:bnb:no_rounding} illustrates that, not rounding or dealing with noise otherwise is not a good idea. Without rounding, the prediction is essentially a step function. The classifier is not better than guessing, no matter how little noise is added.

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{img/noise/BernoulliNB_1_rounding_5.pdf}
\caption{Classification rate of the Bernoulli Naive Bayes classifier in a noisy feature space when rounding to the 5th decimal}
\label{fig:noise:bnb:round5}
\end{figure}

We therefore started rounding the masses before generating the feature vectors and interestingly enough, even though the data has only a precision of $10^{-5}$ when rounding to exactly that, \cref{fig:noise:bnb:round5} displays an entirely different outcome than the first diagram, now producing usable results at the expected noise threshold.

Diagrams were also done for other rounding levels but were not included here to save space since the general pattern is already clear: rounding improves classification precision for noisy data at the trade-off of a lower classification rate for low noise. If rounded to ten, for example, the classification rate is almost constant (70\,\% for the Naive Bayes classifier) for any reasonable noise level.

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{img/noise/BernoulliNB_1_rounding_2.pdf}
\caption{Classification rate of Bernoulli Naive Bayes classifier in a noisy feature space at optimal rounding choice for the expected noise}
\label{fig:noise:bnb:round2}
\end{figure}


The last diagram of this type, \cref{fig:noise:bnb:round2}, displays the optimal choice for this classifier and noise level. This figure also gives a better estimate of the influence of added peaks. Given that most spectra only contain about five to ten peaks, it is interesting to see that adding another five peaks cause the classification rate to decrease by less than 10\,\%.

\begin{figure}[h!]
\centering
\includegraphics[width=\textwidth]{img/noise/BernoulliNB_1_added_peaks_0.pdf}
\caption{Classification rate of Bernoulli Naive Bayes classifier in a noisy feature space with no added peaks at different rounding choices}
\label{fig:noise:bnb:no_peaks}
\end{figure}

If instead of the Noise level, the number of added peaks is kept constant, the result is a diagram similar to  \cref{fig:noise:bnb:no_peaks}. This overview directly displays how a particular rounding choice will probably perform. If the noise level is known, this diagram can serve as a reference to choose the optimal level of rounding required. To display more detail, the data series of not rounding was not displayed, that choice should never be taken on real data. The dotted lines connects are only meant to improve legibility, no functional dependency is implied. In fact, linear and exponential fits have been tried on the data to find an empiric dependency within the data, this however has not proven successful. When generating this diagram for data with added random peaks, the curves are mostly just shifted downwards. The intersections between lines also shift right, meaning that precise rounding outperforms lower rounding at higher noise levels than before.




\subsection{Real data}
\label{real_data_results}
For evaluating our approach in a realistic scenario, the classifiers were trained on the full LipidBlast database and then tested on the real data. The need for cross-validation was hence eliminated by keeping the real data as the constant test set.

Random peaks (chemical noise) were added to the training data. Additionally to the noise parameters used in the previous sections, we examined the impact of thresholds as discussed in section \ref{noise}. The classifications were performed on different parameter combinations using a linear support vector machine.

As noticeable in the following figures, classification rate was generally significantly lower on real data, achieving the best result with an $m/z$ threshold of 300 with 15 added peaks per spectrum rounding to three decimals (the rop right field in figure \ref{fig:real:svm:mz_threshold}) and picking only the four highest-intensity peaks. This resulted in 57.14\% correctly predicted classes.

\begin{figure}[h]
\includegraphics[width=\textwidth]{img/real/rounding_vs_addedpeaks_svm}
\caption{Linear SVM classification rate for real data with an $m/z$ threshold of 300}
\label{fig:real:svm:addedpeaks}
\end{figure}

\begin{figure}[h!]
\includegraphics[width=\textwidth]{img/real/rounding_vs_pickpeaks_svm}
\caption{Linear SVM classification rate for real data with 15 added peaks and an $m/z$ threshold of 300. Several relative intensity thresholds are applied}
\label{fig:real:svm:int_threshold}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{img/real/rounding_vs_threshold_svm}
\caption{Linear SVM classification rate for real data with 15 added peaks, picking the four highest-intensity peaks. The classification rate is encoded by color as indicated by the colorbar}
\label{fig:real:svm:mz_threshold}
\end{figure}

Without adding random peaks to the test set (purple curve in figure \ref{fig:real:svm:addedpeaks}) as well as without applying an $m/z$ threshold (left-most column in figure \ref{fig:real:svm:mz_threshold}) the classification rate often drops to zero. The naive approch of simply using the pure LipidBlast database as the test set can therefore be considered ineffective.

Concerning rounding precision, the results from real data support our previous preference for two to three decimals. We therefore find that estimating the measurement error as Gaussian noise with $\sigma = 0.002$ is a good model. However, we seem to have underestimated the chemical noise since
\begin{itemize}
    \item The best classification result was scored when adding 15 random peaks while we had previously only considered up to five
    \item The classification rates on real data are significantly worse than on simulated data
\end{itemize}

Figure \ref{fig:real:svm:int_threshold} compares the effects of using intensity-based thresholds. While the effect of omitting an intensity threshold is less drastic than omitting the $m/z$ threshold, choosing the right number of peaks benefits classification rate remarkably. The best performance is reached when picking the four highest-intensity peaks. Interestingly, classification is more affected by rounding precision when an intensity threshold is applied since the blue curve in figure \ref{fig:real:svm:int_threshold} is the most constant with all others being worse when rounding to one decimal and better when using two decimals.

\end{document}
